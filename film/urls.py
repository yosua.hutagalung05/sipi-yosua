from django.urls import path

from . import views

app_name = 'film'

urlpatterns = [
    # path('', views.index, name='list'),
    path('', views.FilmView.as_view(), name='list'),
    # path('detail/<int:pk>', views.detail, name='detail'),
    path('detail/<int:pk>', views.FilmDetailView.as_view(), name='detail'),
    # path('create-form/', views.create_form, name='list'),
    path('create-form/', views.AddFilmView.as_view(), name='add'),
    # path('edit-form/<int:pk>', views.edit_form, name='list'),
    path('edit-form/<int:pk>', views.UpdateFilmView.as_view(), name='update'),

    path('delete/<int:pk>', views.DeleteFilmView.as_view(), name = 'delete')
]
