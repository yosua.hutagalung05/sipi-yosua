from django.db import models
import datetime

# Create your models here.


class Film(models.Model):
	judul_film = models.CharField(max_length=150)
	poster = models.URLField(max_length = 500)
	genre = models.CharField(max_length=300)
	jumlah_episode = models.IntegerField()
	sutradara = models.CharField(max_length=150) #sementara karena tidak ada class crew
	tahun_rilis = models.DateField(default=datetime.date.today)
	nama_pemeran = models.CharField(max_length=150) #sementara karena tidak ada class crew
	produser = models.CharField(max_length=150) #sementara karena tidak ada class crew
	skor = models.IntegerField()
	sinopsis = models.TextField()

	def __str__(self):
		return self.judul_film