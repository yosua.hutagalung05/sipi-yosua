from typing import ContextManager
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, DeleteView, CreateView, UpdateView
from .forms import *
from .models import *
from . import forms
from ulasan.models import Reviews
# Create your views here.

class FilmView(ListView):
    model = Film
    template_name = 'film/cb-list.html'

class FilmDetailView(DetailView):
    model = Film
    template_name = 'film/cb-detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(FilmDetailView, self).get_context_data(*args, **kwargs)
        context['reviews']=Reviews.objects.filter(movie=Film.objects.get(pk=self.object.pk))
        return context

class AddFilmView(CreateView):
    model = Film
    form_class = forms.FormAddFilm
    template_name = 'film/cb-add.html'
    def get_success_url(self):
        return reverse_lazy('film:detail', kwargs={'pk': self.object.pk})

class UpdateFilmView(UpdateView):
    model = Film
    form_class = forms.FormAddFilm
    template_name = 'film/cb-update.html'
    def get_success_url(self):
        return reverse_lazy('film:detail', kwargs={'pk': self.object.pk})

class DeleteFilmView(DeleteView):
    model = Film
    success_url = '/film/'
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

