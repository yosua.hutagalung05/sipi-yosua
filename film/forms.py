from django import forms
from .models import Film

class FormAddFilm(forms.ModelForm):
    class Meta:
        model = Film
        fields = {
            'judul_film',
            'poster',
            'genre',
            'jumlah_episode',
            'sutradara',
            'tahun_rilis',
            'nama_pemeran',
            'produser',
            'skor',
            'sinopsis'
        }
        widgets = {

            'judul_film' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'poster' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'genre' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'jumlah_episode' : forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'sutradara' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'tahun_rilis' : forms.DateInput(format='%d/%m/%Y'),
            'nama_pemeran' :forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'produser' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'skor' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'sinopsis' : forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'})
        }

	
	
	
	
	
	
	
	