from django import forms
from .models import WatchlistFilm

class WatchlistFilmStatusForm(forms.ModelForm):
    class Meta:
        model = WatchlistFilm
        fields = ['status_menonton']
        widgets = {
             'status_menonton' : forms.Select(attrs={'class': 'custom-select', 'id': 'inputGroupSelect01'}, choices=[('True', 'Sudah selesai'), ('False', 'Belum Selesai')]),
        }
        

class WatchlistFilmSkorForm(forms.ModelForm):
    class Meta:
        model = WatchlistFilm
        fields = ['skor_ulasan']