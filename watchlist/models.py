from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from account.models import User
from film.models import Film

class Watchlist(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None) # Silakan ditambah argumennya nanti

class WatchlistFilm(models.Model):
    film = models.ForeignKey(Film, on_delete=models.CASCADE)
    watchlist = models.ForeignKey(Watchlist, on_delete=models.CASCADE)
    skor_ulasan = models.IntegerField(
        default = 0,
        validators = [
            MaxValueValidator(10),
            MinValueValidator(0)
        ]
    )
    status_menonton = models.BooleanField(default = False)
    jml_episode_tertonton = models.IntegerField(default = 0)