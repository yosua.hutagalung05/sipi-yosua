from django.contrib import admin
from .models import Watchlist, WatchlistFilm

admin.site.register(Watchlist)
admin.site.register(WatchlistFilm)
