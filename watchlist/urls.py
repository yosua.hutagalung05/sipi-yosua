from django.urls import path
from . import views

app_name = 'watchlist'

urlpatterns = [
    path('watchlist/', views.get_watchlist, name='get_watchlist'),
    path('watchlistfilm/<int:pk>/tambah_episode_tertonton/', views.tambah_episode_tertonton, name='tambah_episode_tertonton'),
    path('watchlistfilm/<int:pk>/kurang_episode_tertonton/', views.kurang_episode_tertonton, name='kurang_episode_tertonton'),
    path('watchlistfilm/<int:pk>/hapus', views.delete_watchlist_film, name="delete_watchlist_film"),
    path('watchlistfilm/<int:pk>/update_status_menonton', views.update_status_menonton_watchlist_film, name='update_status_menonton'),
    path('watchlist/tambah_film/<int:film_pk>', views.tambah_film, name="tambah_film_watchlist"),
    path('watchlist/batalkan_tambah_film/<int:film_pk>', views.batalkan_tambah_film, name='batalkan_tambah_film')
]