from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import request
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from .models import Watchlist, WatchlistFilm
from .forms import WatchlistFilmSkorForm, WatchlistFilmStatusForm
from film.models import Film

@login_required(login_url='account:login')
def get_watchlist(request):
    watchlist = Watchlist.objects.get(user=request.user)
    watchlist_films = WatchlistFilm.objects.filter(watchlist=watchlist.pk)
    update_status_form = WatchlistFilmStatusForm(request.POST or None)

    if request.method == 'GET':
        response = {
            'watchlist': watchlist,
            'watchlist_films': watchlist_films,
            'update_status_form': update_status_form,
        }
        return render(request, 'index.html', response)

@login_required(login_url='account:login')
def update_status_menonton_watchlist_film(request, pk):
    watchlist_film = WatchlistFilm.objects.get(pk=pk)

    if request.method == 'POST':
        status_menonton = request.POST.get('status_menonton')
        print
        if status_menonton == 'Sudah':
            watchlist_film.status_menonton = True
        else:
            watchlist_film.status_menonton = False

        watchlist_film.save()
        
        return HttpResponseRedirect('/watchlist/')

@login_required(login_url='account:login')
def update_skor_ulasan_watchlist_film(request, pk):
    watchlist_film = WatchlistFilm.objects.get(pk=pk)
    form = WatchlistFilmSkorForm(request.POST or None, instance=watchlist_film)

    if request.method == 'POST' and form.is_valid():
        form.save()
        return HttpResponseRedirect() # Silakan ganti dengan halaman yang diinginkan

@login_required(login_url='account:login')
def delete_watchlist_film(request, pk):
    watchlist_film = WatchlistFilm.objects.get(pk=pk)
    
    if request.method == 'POST':
        watchlist_film.delete()
        return HttpResponseRedirect('/watchlist/')

@login_required(login_url='account:login')
def tambah_episode_tertonton(request, pk):
    watchlist_film = WatchlistFilm.objects.get(pk=pk)

    if request.method == 'POST':
        watchlist_film.jml_episode_tertonton += 1
        watchlist_film.save()
        return HttpResponseRedirect('/watchlist/')

@login_required(login_url='account:login')
def kurang_episode_tertonton(request, pk):
    watchlist_film = WatchlistFilm.objects.get(pk=pk)

    if request.method == 'POST':
        watchlist_film.jml_episode_tertonton -= 1
        watchlist_film.save()
        return HttpResponseRedirect('/watchlist/')

@login_required(login_url='account:login')
def tambah_film(request, film_pk):
    film = Film.objects.get(pk=film_pk)
    watchlist = Watchlist.objects.get(user=request.user)
    
    if request.method == 'POST':
        watchlist_film = WatchlistFilm(film=film, watchlist=watchlist)
        watchlist_film.save()

        messages.add_message(request, messages.SUCCESS, "Film berhasil ditambahkan.")

        return HttpResponseRedirect('/film/detail/' + str(film_pk))

@login_required(login_url='account:login')
def batalkan_tambah_film(request, film_pk):
    film = Film.objects.get(pk=film_pk)
    watchlist = Watchlist.objects.get(user=request.user)
    
    if request.method == 'POST':
        watchlist_film = WatchlistFilm.objects.filter(film=film, watchlist=watchlist)
        watchlist_film.delete()

        messages.add_message(request, messages.INFO, "Aksi dibatalkan.")

        return HttpResponseRedirect('/film/detail/' + str(film_pk))