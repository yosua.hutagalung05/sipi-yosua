from ulasan.models import Reviews
from film.models import Film
from django.shortcuts import render, redirect
from ulasan.forms import ReviewForm

from django.contrib import messages
from django.contrib.auth.decorators import login_required
# Create your views here.


def seeulasan(request, pk):
    movie = Film.objects.get(pk=pk)

    if(not request.user.is_authenticated):
        messages.add_message(request, messages.WARNING, 'Please Log In')
        return redirect('home:home')

    reviewlist = Reviews.objects.filter(movie=movie, author=request.user)
    if not reviewlist:
        return redirect('/ulasan/formulasan/'+str(pk))

    review = Reviews.objects.get(movie=movie, author=request.user)
    context = {'pk': pk, 'movie': movie, 'review': review}

    return render(request, 'ulasan/seeulasan.html', context)


def makereview(request, pk):
    form = ReviewForm()
    movie = Film.objects.get(pk=pk)
    context = {'pk': pk, 'form': form, 'movie': movie}

    if(not request.user.is_authenticated):
        messages.add_message(request, messages.WARNING, 'Please Log In')
        return redirect('home:home')

    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(commit=False)
            review.movie = movie
            review.author = request.user
            review.save()
            return redirect('/ulasan/seeulasan/'+str(pk))
        else:
            context['form'] = ReviewForm()
    review = Reviews.objects.filter(movie=movie, author=request.user)
    if review:
        return redirect('home:home')
    return render(request, 'ulasan/formulasan.html', context)


def updatereview(request, pk):
    movie = Film.objects.get(pk=pk)
    review = Reviews.objects.get(movie=movie, author=request.user)
    form = ReviewForm(instance=review)

    context = {'pk': pk, 'form': form, 'movie': movie}

    if(not request.user.is_authenticated):
        messages.add_message(request, messages.WARNING, 'Please Log In')
        return redirect('home:home')

    if request.method == 'POST':
        form = ReviewForm(request.POST or None, instance=review)
        if form.is_valid():
            form.save()
            return redirect('/ulasan/seeulasan/'+str(pk))
        else:
            context['form'] = form = ReviewForm(
                request.POST or None, instance=review)
    return render(request, 'ulasan/updateulasan.html', context)


def deletereview(request, pk):
    movie = Film.objects.get(pk=pk)
    review = Reviews.objects.get(movie=movie, author=request.user)
    context = {'movie':movie, 'review':review, 'pk':pk}

    if (request.method == 'POST'):
        review.delete()
        return redirect('film:list')
    return render(request, 'ulasan/confirmdeleteul.html', context)
