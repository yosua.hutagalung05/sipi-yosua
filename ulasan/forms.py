from django import forms
from django.forms import ModelForm
from django.forms.widgets import Textarea
from ulasan.models import Reviews

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Reviews
        fields = [
            'rating',
            'body'
        ]

        widgets = {
            'rating': forms.TextInput(attrs={'class':'form-control', 'placeholder':'rating / 10'}),
            'body': forms.Textarea(attrs={'class':'form-control'})
        }