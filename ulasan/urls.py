from django.urls import path

from . import views

app_name = 'ulasan'

urlpatterns = [
    path('seeulasan/<int:pk>', views.seeulasan, name='seeulasan'),
    path('formulasan/<int:pk>', views.makereview, name='formulasan'),
    path('updateulasan/<int:pk>', views.updatereview, name='updateulasan'),
    path('confdeleulasan/<int:pk>', views.deletereview, name='deleteulasan'),
]
