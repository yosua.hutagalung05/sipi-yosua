from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from film.models import Film

User = settings.AUTH_USER_MODEL
# Create your models here.
class Reviews(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey(Film, blank=True, null=True, on_delete=models.CASCADE)
    rating = models.IntegerField(validators=[MaxValueValidator(10), MinValueValidator(0)])
    body = models.TextField()
    
    def __str__(self):
        return (str(self.movie) + '-' + str(self.author))