from django.db import models
from django.conf import settings
from film.models import Film
from django.utils import timezone

User = settings.AUTH_USER_MODEL
# Create your models here.

class Forum(models.Model):
	film = models.ForeignKey(Film, blank=True, null=True, on_delete=models.CASCADE)
	user = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
	topik = models.CharField(max_length=200, blank=True)
	isi = models.TextField(max_length=500, null=True, blank=True)
	tanggal = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.topik

class Reply(models.Model):
	user = models.ForeignKey(User, blank=True, on_delete=models.CASCADE)
	forum = models.ForeignKey(Forum, blank=True, on_delete=models.CASCADE)
	isi = models.TextField(max_length=500, null=True, blank=True)
	tanggal = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.user.username + ": " + self.forum.topik
	class Meta:
		db_table = 'reply'
		verbose_name_plural = "replies"