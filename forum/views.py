from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import *
from .models import *
from film.models import Film
from django.contrib.auth.decorators import login_required

# Create your views here.
def forum_view(request, id):
	film = Film.objects.get(id=id)
	forum = film.forum_set.all()
	context ={'film': film, 'forums': forum}
	return render(request, 'forum_detail.html', context)

@login_required(login_url='account:login')
def add_new_topic_forum(request, id):
	form = ForumForm()
	film = Film.objects.get(id=id)
	context={'id': id, 'form': form, 'film': film}
	if(request.POST):
		data = request.POST
		form = ForumForm(request.POST)
		if form.is_valid():
			Forum.objects.create(user=request.user, film=film, topik=data['topik'], isi=data['isi'])
			messages.add_message(request, messages.INFO,'Forum has been created')
			return redirect('forum:forum_view', id=id)
	else:
		context['form'] = ForumForm()
	return render(request, 'add_new_topic_forum.html', context)


def forum_view_replies(request, id, idForum):
	form = ReplyForm()
	forum = Forum.objects.get(id=idForum)
	film = Film.objects.get(id=id)
	replies = forum.reply_set.all()
	context = {'idFilm': id, 'idForum': idForum, 'form': form, 'film': film, 'replies': replies, 'forum': forum}
	if(request.POST and request.user.is_authenticated):
		data = request.POST
		form = ReplyForm(request.POST)
		if form.is_valid():
			forum = Forum.objects.get(id=idForum)
			Reply.objects.create(user=request.user, forum=forum, isi=data['isi'])
			messages.add_message(request, messages.INFO,'Reply has been added')
			return redirect('forum:view-replies', id=id, idForum=idForum)
	else:
		context['form'] = ReplyForm()
	return render(request, 'forum_replies.html', context)

@login_required(login_url='account:login')
def delete_forum(request, id):
	forum = Forum.objects.get(id=id)
	idFilm = forum.film.id
	forum.delete()
	messages.add_message(request, messages.INFO,'Forum has been deleted')
	return redirect('forum:forum_view', id=idFilm)

@login_required(login_url='account:login')
def delete_reply(request, id):
	reply = Reply.objects.get(id=id)
	idFilm = reply.forum.film.id
	idForum = reply.forum.id
	reply.delete()
	messages.add_message(request, messages.INFO,'Reply has been deleted')
	return redirect('forum:view-replies', id=idFilm, idForum=idForum)
