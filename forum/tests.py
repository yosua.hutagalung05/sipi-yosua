from django.test import TestCase
from django.apps import apps
from .models import *
from .apps import ForumConfig
# Create your tests here.
class ForumConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(ForumConfig.name, 'forum')
        self.assertEqual(apps.get_app_config('forum').name, 'forum')