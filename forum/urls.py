from django.contrib import admin
from django.urls import path
from . import views

app_name = 'forum'

urlpatterns = [
    path('forum/<int:id>',views.forum_view, name='forum_view'),
    path('add-new-topic-forum/<int:id>',views.add_new_topic_forum, name='add_new_topic_forum'),
    path('view-replies/<int:id>/<int:idForum>',views.forum_view_replies, name='view-replies'),
    path('del-forum/<int:id>',views.delete_forum, name='del-forum'),
    path('del-reply/<int:id>',views.delete_reply, name='del-reply'),

]