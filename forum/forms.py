from django import forms
from .models import *

class ForumForm(forms.ModelForm):
	isi = forms.CharField(max_length=500, widget=forms.Textarea, required=False)
	topik = forms.CharField(max_length=200, required=True)
	class Meta:
		model = Forum
		fields = ['topik', 'isi']

class ReplyForm(forms.ModelForm):
	isi = forms.CharField(max_length=500, widget=forms.Textarea, required=True)

	class Meta:
		model = Reply
		fields = ['isi']