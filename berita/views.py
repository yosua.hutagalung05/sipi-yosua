from typing import ContextManager
from django.http.response import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, DeleteView, CreateView, UpdateView
from .models import Berita
from . import forms

# Create your views here.

class ArticleView(ListView):
    model = Berita
    template_name = 'berita/cb-list.html'

class ArticleDetailView(DetailView):
    model = Berita
    template_name = 'berita/cb-detail.html'

class AddArticleView(CreateView):
    model = Berita
    form_class = forms.FormAddArticle
    template_name = 'berita/cb-add.html'
    def get_success_url(self):
        return reverse_lazy('berita:detail', kwargs={'pk': self.object.pk})

class UpdateArticleView(UpdateView):
    model = Berita
    form_class = forms.FormAddArticle
    template_name = 'berita/cb-update.html'
    def get_success_url(self):
        return reverse_lazy('berita:detail', kwargs={'pk': self.object.pk})

class DeleteArticleView(DeleteView):
    model = Berita
    success_url = '/berita/'
    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)
