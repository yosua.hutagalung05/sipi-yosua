from django.db import models
import datetime

# Create your models here.
class Berita(models.Model):
    judul_artikel = models.CharField(max_length=100)
    penulis = models.CharField(max_length=75)
    tanggal = models.DateField(default=datetime.date.today)
    tautan_ke_gambar = models.URLField(max_length = 300)
    isi_artikel = models.TextField()
    sumber = models.CharField(max_length=100)