from django import forms
from .models import Berita

class FormAddArticle(forms.ModelForm):
    class Meta:
        model = Berita
        fields = {
            'judul_artikel',
            'penulis',
            'tanggal',
            'tautan_ke_gambar',
            'isi_artikel',
            'sumber'
        }
        widgets = {
            'judul_artikel' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'penulis' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'tanggal' : forms.DateInput(format='%d/%m/%Y'),
            'tautan_ke_gambar' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'isi_artikel' : forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'}),
            'sumber' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'tulis di sini'})
        }
