from django.urls import path

from . import views

app_name = 'berita'

urlpatterns = [
    path('', views.ArticleView.as_view(), name='list'),
    path('detail/<int:pk>', views.ArticleDetailView.as_view(), name='detail'),
    path('create-form/', views.AddArticleView.as_view(), name='add'),
    path('edit-form/<int:pk>', views.UpdateArticleView.as_view(), name='update'),
    path('delete/<int:pk>', views.DeleteArticleView.as_view(), name = 'delete')
]