from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib import messages
# Source: https://github.com/divanov11/crash-course-CRM/tree/Part---15---User-Role-Based-Permissions-%26-Authentication

def unauthenticated_user(view_func):
	def wrapper_func(request, *args, **kwargs):
		if request.user.is_authenticated:
			return redirect('home:home')
		else:
			return view_func(request, *args, **kwargs)

	return wrapper_func

def allowed_users(allowed_roles=[]):
	def decorator(view_func):
		def wrapper_func(request, *args, **kwargs):

			group = None
			if request.user.group.exists():
				group = request.user.group.all()[0].name

			if group in allowed_roles:
				return view_func(request, *args, **kwargs)
			else:
				 messages.add_message(request, messages.INFO,"Permission Denied")
				 return redirect('home:home')
		return wrapper_func
	return decorator

def admin_only(view_func):
	def wrapper_function(request, *args, **kwargs):
		group = None
		if request.user.group.exists():
			group = request.user.group.all()[0].name

		if group == 'anggota':
			return redirect('home:home')

		if group == 'maintainer':
			return view_func(request, *args, **kwargs)

	return wrapper_function