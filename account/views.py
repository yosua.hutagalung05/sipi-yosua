from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from account.forms import RegistrationForm, LoginForm
import json
from django.contrib.auth.models import Group
from django.contrib import messages
from django.http import JsonResponse
from .decorators import unauthenticated_user, allowed_users
from watchlist.models import Watchlist


@unauthenticated_user
def loginPage(request):
    form = LoginForm()
    context = {}
  
    if request.POST:
        form = LoginForm(request.POST)
        if form.is_valid():
            email = request.POST["email"]
            password = request.POST["password"]
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                messages.add_message(request, messages.INFO,
                                 "You're logged in")
                return redirect("home:home")
            else:
                context['form'] = LoginForm()
    else:
        context['form'] = LoginForm()
    context['form'] = form
    return render(request, 'registration/login.html', context)

@unauthenticated_user
def register(request):
    context = {}
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            group = form.cleaned_data['group']        
            my_group = Group.objects.get(name=group) 
            my_group.user_set.add(user)
            watchlist = Watchlist(user=user)
            watchlist.save()
            messages.add_message(request, messages.INFO,
                                 "You're Registered, Please Log In")
            return redirect("account:login")
        else:
            context['form'] = form
    else:
        form = RegistrationForm()
        context['form'] = form
    return render(request, 'registration/register.html', context)


def logoutPage(request):
    logout(request)
    messages.add_message(request, messages.INFO,
                                 "You're Logged Out")
    return redirect("home:home")