from django import forms
from django.contrib.auth.forms import UserCreationForm
from account.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.models import Group


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(
        max_length=40, help_text="Required a valid email address")
    group = forms.ModelChoiceField(queryset=Group.objects.all(),
                                   required=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'password1',
                  'password2', 'first_name', 'last_name', 'group']


class LoginForm(forms.ModelForm):
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ("email", "password")

    def clean(self):
        email = self.cleaned_data.get("email")
        password = self.cleaned_data.get("password")
        user = authenticate(email=email, password=password)
        if not user:
            raise forms.ValidationError("email or password is invalid")
